# Bruce's Personal Website - IDS721

Welcome to the repository for my personal website, created for IDS721 at Duke University in Spring 2024. This site showcases my academic projects, blog posts, and more.

## Author

- **Bruce Xu**
- NetID: zx112
- [GitHub](https://github.com/zilin2000)

## Website url
https://week1-zola-website-bruce1018-743ef14ef2b10f6a6a7153b06a0c3fdbdf.gitlab.io/

## Website overview

![截屏2024-02-01 18.47.54.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/171adde722724cff96644d3dcc9242ab~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=2868&h=1512&s=532038&e=png&b=27272c)

## Tech Stack

This website is powered by [Zola](https://www.getzola.org/), a fast static site generator in a single binary with everything built-in.

## Structure

- content/: Markdown files for the website's content.
- static/: Static files like images and CSS.
- templates/: HTML templates for the site's layout.
